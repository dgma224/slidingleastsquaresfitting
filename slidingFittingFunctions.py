'''
This file contains the various fit function routines that were described in the paper.

In addition to the fit function routines, the functions used for performance testing are also available here so users of this methodogy can perform their own testing to determine the optimal deployment of the method for their purposes. 

This code is free to use and adapt under the MIT open license. 

If you have any questions about the code, please contact the owner David Mathews at mathewsdg@ornl.gov
'''

import numpy as np #the usual
import numba

import gc
import warnings
warnings.filterwarnings('ignore') #used to ignore some of the numba warnings that appear occasionally

__fftwAvailable = False #used to check if the user has pyFFTW installed. If they do it will use that library for the FFT calculations as it is much faster than numpy's implementation
useFFTW = False
try:
	import pyfftw
	__fftwAvailable = True
	useFFTW = True
except Exception as e:
	__fftwAvailable = False
	useFFTW = False

#try to configure the GPU to see if that can be used or not
__gpuAvailable = False
__gpuErrorMessage = None
try:
	import gpuFunctions as gpu
	__gpuAvailable = True
except Exception as e:
	print(e)
	__gpuAvailable = False
	__gpuErrorMessage = e

'''
Define some arbitrary noise profiles.
Code taken from https://stackoverflow.com/questions/67085963/generate-colors-of-noise-in-python
'''
def noise_psd(N, psd = lambda f: 1):
        X_white = np.fft.rfft(np.random.randn(N));
        S = psd(np.fft.rfftfreq(N))
        # Normalize S
        S = S / np.sqrt(np.mean(S**2))
        X_shaped = X_white * S;
        return np.fft.irfft(X_shaped);

def PSDGenerator(f):
    return lambda N: noise_psd(N, f)

@PSDGenerator
def white_noise(f):
    return 1;

@PSDGenerator
def blue_noise(f):
    return np.sqrt(f);

@PSDGenerator
def violet_noise(f):
    return f;

@PSDGenerator
def brownian_noise(f):
    return 1/np.where(f == 0, float('inf'), f)

@PSDGenerator
def pink_noise(f):
    return 1/np.where(f == 0, float('inf'), np.sqrt(f))

@numba.jit
def simpleSignalGenerator(length: int, t0: int, decay: float, amplitude: float, noise = 'pink', rms = 1.0):
	'''
	A simple exponentiallyl decaying signal generator that supports some different noise types
	
	Parameters:
		length: int
			The length of the output signal
		t0: int
			The start position of the signal
		decay: float
			The decay rate of the exponential signal
		amplitude: float
			The amplitude of the signal
		noiseType: string
			The type of noise. Supported options are: white, blue, violet, brownian, pink
			Any input not matching these types will result in a noiseless signal
		rms: float, 1.0
			the rms amplitude of the noise added to the waveform
	Output:
		signal: np.ndarray
			A numpy array defined as 64-bit floating point values containing the signal
	'''
	x = np.arange(length)
	y = np.zeros(length)
	y[t0:] = np.exp(-1.0*(x[t0:]-t0)/decay)*amplitude
	if isinstance(noise, str):
		if noise == 'white':
			y += white_noise(length)*rms
		elif noise == 'blue':
			y += blue_noise(length)*rms
		elif noise == 'violet':
			y += violet_noise(length)*rms
		elif noise == 'brown' or noise == 'brownian':
			y += brownian_noise(length)*rms
		elif noise == 'pink':
			y += pink_noise(length)*rms
	return y

@numba.jit
def trapezoidalFilterRecursive(inp, rise_time, flat_top, tau):
	"""
	This function applies the trapezoidal filter via recursion to an input trace. This is a direct translation of the algorithm in the Jordanov paper here: 
	
	Nuclear Instruments and Methods in Physics Research A353 (1994) 261-264
	https://deepblue.lib.umich.edu/bitstream/handle/2027.42/31113/0000009.pdf?sequence=1
	
	"""
	d = 0
	rise_time = int(rise_time)
	flat_top = int(flat_top)
	p = np.zeros(len(inp))
	s = np.zeros(len(inp))
	p[0]=s[0]=inp[0]
	tau = 1.0/(np.exp(1.0/tau)-1.0)
	for i in range(1, len(inp)):
		if i>=2*rise_time+flat_top:
			d = inp[i]-inp[i-rise_time]-inp[i-rise_time-flat_top]+inp[i-2*rise_time-flat_top]
		else:
			if i>=rise_time+flat_top:
				d = inp[i]-inp[i-rise_time]-inp[i-rise_time-flat_top] 
			else:
				if i>=rise_time:
					d = inp[i]-inp[i-rise_time]
				else:
					d = inp[i];
		p[i]=p[i-1]+d
		s[i]=s[i-1]+p[i]+tau*d
	return s[:]/(rise_time * tau)

def defineSingleTrap(rise, top, tau, length=None):
	"""
	This function defines the standard trapezoidal filter as discussed in the Jordanov paper in such a way as to be used in a convolution based filtering approach. That paper defines it for recursive implementations, this is a convolution based implementation more suited for Python and use with convolution functions such as numpy's np.convolve or np.fftconvlve.

	Nuclear Instruments and Methods in Physics Research A353 (1994) 261-264
	https://deepblue.lib.umich.edu/bitstream/handle/2027.42/31113/0000009.pdf?sequence=1

	Parameters
	----------
	rise: int
		this parameter controls the length of the rising edge of the trapezoid. This should be tuned and optimized based on the noise in the system. A good initial guess is the same value as tau.
	top: int
		the length of the flat top of the trapezoid. This should be longer than the expected rising edge of the pulse shape to properly integrate accumulated charge.
	tau: int, float
		the decay constant of the electronics. This should be a fixed value determined by the electronics chain. This can be determined by fitting the decaying region of multiple waveforms to an exponentially decaying function and extracting the decay rate
	length: int (defaults to None)
		the desired length of the filter. This needs to be at least 2 * rise + top and can be longer if padding is desired for the filtering process (some convolution based filtering methods need the filter and waveform to be the same size)
		If no input is provided, this defaults to None and the filter is not padded
	Returns
	-------
	filter: np.ndarray
		This returns the filter defined in a numpy array ready to be convolved with the waveform. It is pre-scaled such that the output from using this filter with a normalized convolution methodology will not artificially scale the waveform amplitude. With some FFT-based convolution techniques, additional scaling may be necessary if the method isn't already normalized.
	"""
	filt = None
	if length is None:
		filt = np.zeros(rise * 2 + top)
	else:
		if length < rise * 2 + top:
			print('invalid length parameter')
			print('should be longer than rise * 2 + top')
			return None
		else:
			filt = np.zeros(length)
	for i in range(rise):
		filt[i]=i+tau
		filt[i+rise+top]=rise-tau-i
	for i in range(rise,rise+top):
		filt[i]=rise
	for i in range(rise+rise+top,len(filt)):
		filt[i]=0
	scale=1.0/(rise*tau)
	filt*=scale
	return filt

@numba.jit
def extractTrapResults(filtered, rise, top, percentage, shift = 0, mean = 0):
	"""
	This function extracts the results from the trapezoidal filter. It does so with a percent threshold cross algorithm so it looks for a maximum, identifies the filter output information, and extracts timing and energy information from this.
	
	Parameters
	----------
	filtered: np.ndarray
		The output of the convolution of the waveform and the filter
	rise: int
		the rise time parameter for the trapezoidal filter
	top: int
		the flat top length for the trapezoidal filter
	percentage: float(0 through 1)
		the percent threshold used 
	
	Returns
	-------
	energy: float
		the energy extracted from the waveform
	timing: float
		the start time of the waveform
	"""
	maxloc = np.argmax(filtered)
	maxval = filtered[maxloc]
	threshold = maxval * percentage
	#find upwards cross point
	#iterate from the max location back towards beginning
	i = maxloc
	found = False
	leftCross = None
	while i >= maxloc - rise - top and found == False:
		if filtered[i] >= threshold and filtered[i-1] <= threshold:
			found = True 
			leftCross = i-1
		i-= 1
	rightCross = None
	found = False
	i = maxloc
	while i <= maxloc + rise + top and found == False:
		if filtered[i] >= threshold and filtered[i+1] <= threshold:
			found = True
			rightCross = i + 1
		i+=1
	#now use these to extract the results
	if rightCross is None or leftCross is None:
		return np.nan, np.nan
	else:
		midpoint = (leftCross + rightCross)/2
		if mean == 0:
			return filtered[int(midpoint)+shift], midpoint - top/2 - rise
		else:
			if mean < 0:
				return filtered[int(midpoint+shift-mean):int(midpoint+shift+1)].mean(), midpoint - top/2 - rise
			else:
				return filtered[int(midpoint+shift):int(midpoint+shift+mean)].mean(), midpoint - top/2 - rise
	return 0, 0

def pyFFTWExtraction(waveforms, pretrigger, fftFilt, padLen, extractionFunc, args, batchSize = -1, block_info = None):
	"""
	This is a more efficient form of convolution using the FFTW library. 
	It's more efficient than the standard numpy/scipy/dask implementations
	"""
	numWaves, waveformLength = waveforms.shape
	if batchSize <= 0 or batchSize >= numWaves:
		batchSize = numWaves
	numBatches = numWaves//batchSize
	if numWaves % batchSize != 0:
		numBatches += 1
	#prep the work environment for the stuff
	#create the buffers that'll be used for the FFTs
	padded = np.zeros(shape=(batchSize, padLen), dtype=np.float32)
	fftArray = np.zeros(shape=[batchSize, padLen//2+1], dtype=np.complex64)
	#create the FFT plans
	fftForward = pyfftw.FFTW(padded, fftArray, direction='FFTW_FORWARD', axes=[1])
	fftReverse = pyfftw.FFTW(fftArray, padded, direction='FFTW_BACKWARD', axes=[1])
	#create the results array 
	results = np.zeros(shape=(numWaves, 2))
	for i in range(numBatches):
		startLoc = batchSize * i
		stopLoc = batchSize * (i+1)
		if stopLoc > numWaves:
			stopLoc = numWaves
		padded[:stopLoc-startLoc,:waveformLength] = waveforms[startLoc:stopLoc,:] 
		means = padded[:stopLoc-startLoc,0:pretrigger].mean(axis=1)
		padded[:stopLoc-startLoc,:waveformLength] = np.subtract(padded[:stopLoc-startLoc,:waveformLength], means[:,None])
		fftForward() #do their FFT, now the FFT results are stored in the fftArray array
		#fftArray *= fftFilt #do the multiplication in place or as in place as python allows
		np.multiply(fftArray, fftFilt, out=fftArray)
		fftReverse() #now undo the FFT back
		#fft has now been applied, apply the extraction function
		res = np.apply_along_axis(extractionFunc, 1, padded, *args)
		results[startLoc:stopLoc,0] = np.copy(res[:stopLoc-startLoc,0])
		results[startLoc:stopLoc,1] = np.copy(res[:stopLoc-startLoc,1])
		padded[:,:] = 0 #reset it to 0
	del padded
	del fftArray
	del fftFilt
	del means
	del res
	del fftForward
	del fftReverse
	del waveforms
	return results

@numba.jit
def cuspFilterRecursive(inp, rise_time, flat_top, tau):
	length = len(inp)
	filter_length = rise_time*2+flat_top
	out = np.zeros(length+filter_length)
	p0_1 = np.zeros(length+filter_length) #0th-order polynomial, segment 1
	p1_1 = np.zeros(length+filter_length) #1st-order polynomial, segment 1
	p2_1 = np.zeros(length+filter_length) #2nd-order polynomial, segment 1
	p0_2 = np.zeros(length+filter_length) #0th-order polynomial, segment 2
	p0_3 = np.zeros(length+filter_length) #0th-order polynomial, segment 3
	p1_3 = np.zeros(length+filter_length) #1st-order polynomial, segment 3
	p2_3 = np.zeros(length+filter_length) #2nd-order polynomial, segment 3
	c0_1 = 0.5*(tau-0.5) # coefficient for p0_1
	c1_1 = (tau-0.5) # coefficient for p1_1
	c2_1 = 1.0 #coefficient for p2_1
	c0_2 = 0.5*rise_time*(rise_time+1.) #coefficient for p0_2
	c0_3 = .5*(rise_time+rise_time*rise_time-(tau-0.5)-2.*rise_time*(tau-0.5)) #coefficient for p0_3
	c1_3 = -1.-rise_time+(tau-0.5) #coefficient for p1_3
	c2_3 = 1.0 #coefficient for p2_3
	for i in range(length):
		p0_1[i+filter_length] = p0_1[i+filter_length-1] + inp[i] - (inp[i-rise_time] if i>=rise_time else 0)
		p1_1[i+filter_length] = p1_1[i+filter_length-1] + p0_1[i+filter_length] - (rise_time*inp[i-rise_time] if i>=rise_time else 0)
		p2_1[i+filter_length] = p2_1[i+filter_length-1] + p1_1[i+filter_length] - (.5*rise_time*(rise_time+1)*inp[i-rise_time] if i>=rise_time else 0)

		p0_2[i+filter_length] = p0_2[i+filter_length-1] + (inp[i-rise_time] if i>=rise_time else 0.) - (inp[i-rise_time-flat_top] if i>=(rise_time+flat_top) else 0.)

		p0_3[i+filter_length] = p0_3[i+filter_length-1] + (inp[i-rise_time-flat_top] if i>=(rise_time+flat_top) else 0) - (inp[i-filter_length] if i>=filter_length else 0.0)
		p1_3[i+filter_length] = p1_3[i+filter_length-1] + p0_3[i+filter_length] - (rise_time*inp[i-filter_length] if i>=filter_length else 0.)
		p2_3[i+filter_length] = p2_3[i+filter_length-1] + p1_3[i+filter_length] - (.5*rise_time*(rise_time+1)*inp[i-filter_length] if i>=filter_length else 0.)

		out[i] = (c0_1*p0_1[i+filter_length]+c1_1*p1_1[i+filter_length]+c2_1*p2_1[i+filter_length]+c0_2*p0_2[i+filter_length]+c0_3*p0_3[i+filter_length]+c1_3*p1_3[i+filter_length]+c2_3*p2_3[i+filter_length])/(.5*rise_time*(rise_time+1.)*(tau-0.5));
	return out[:length]

def defineCuspFilter(rise_time, flat_top, tau, length = None):
	"""
	Defines the standard cusp filter. Check this paper for possibly useful information.
	https://arxiv.org/abs/1504.02039
	Definition taken from Aaron Jezghani's DSP gitlab
	https://gitlab.com/apjezghani/DigitalSignalProcessing
	
	Parameters
	----------
	rise_time: int
		the shaping time of the filter. Similar to the rise time parameter in the trapezoidal filter
	flat_top: int
		length of the charge integration time. Similar to the flat top parameter for the trapezoidal filter
	tau: int, float
		the decay rate parameter. Should be tuned to match the decay rate of the waveforms
	Returns
	-------
	filter: np.ndarray
		The filter defined for convolution based implementations
	"""
	inp = np.zeros(length)
	inp[0] = 1.0
	return cuspFilterRecursive(inp, rise_time, flat_top, tau)[:length]

def extractCuspResults(filtered, rise, top, percentage, shift = 0, mean = 0):
	"""
	This function extracts the energy and timing information from the cusp filter output
	It wraps around the Trapezoidal Filter Energy Extraction as they are the same algorithm
	Parameters
	----------
	filtered: np.ndarray
		the filtered output of the waveform with the cusp filter
	rise: int
		the rise time provided to the cusp filter. This is used to correct for the timing
	
	Returns
	-------
	energy: float
		the peak amplitude of the waveform
	timing: int
		the start time of the waveform
	"""
	return extractTrapResults(filtered, rise, top, percentage, shift = shift, mean = mean)


def testFIRFilterRecursive(waves, rise, top, tau, filt = 'trap', pretrigger=800, batchSize = 1000, useGPU = False, percentage = 0.8, mean = 0, shift = 0):
	"""
	This function applies either the trapezoidal filter, or cusp filter, to a batch of waveforms and extracts energy and timing information with it. This version of the filter is defined via recursion.
	For a description of the trapezoidal filter defined here, check this resource.
	Nuclear Instruments and Methods in Physics Research A353 (1994) 261-264
	https://deepblue.lib.umich.edu/bitstream/handle/2027.42/31113/0000009.pdf?sequence=1
	
	Parameters
	----------
	waves: np.ndarray
		a 2d numpy array with waveforms in it
	rise: int
		the risetime parameter for the trapezoidal filter
	top: int
		the flat top length of the trapezoidal filter
	tau: int,float
		the decay rate parameter for the trapezoidal filter
	filt: string
		The filter to be used. Supports either 'cusp' or 'trap' options
	pretrigger: int
		the length of the waveform to be used to average the baseline to 0
	batchSize: int
		how many waveforms to process at a time. Defaults to 1,000 but should be optimized on a per-computer basis. -1 means do all at once (warning this can use a lot of RAM)
		Only used on the GPU, no impact on CPU
	useGPU: bool
		Changes the code to operate on the GPU instead through Cupy library and Numba GPU compilation
	percentage: float between 0 and 1, defaults to 0.8
		The threshold cross percentage
	mean: int, defaults to 0
		The number of values to average over for energy extraction
	shift: int (defaults to 0)
		applies a shift to the extraction location for the energy from the trap filter
	
	Returns
	-------
	energy: list
		list of floating point numbers that define the energies of the waveforms
	timing: list
		list of extracted start time of waveforms
	"""
	if waves.shape[0] == 0:
		return [], []
	else:
		pretrigger = int(pretrigger)
		#this applies the trapezoidal filter to the waveforms and grabs the energy and timing results
		eners = []
		times = []
		#do the bulk wave preparation
		if batchSize == -1:
			batchSize = len(waves)
		numWaves = len(waves)
		leftover = numWaves % batchSize
		numbatches = int(numWaves / batchSize)
		#set up the main computations
		if useGPU == True and __gpuAvailable == True: #only try it when it's available and requested
			results = gpu.gpuTrapCuspFilterImplementationFFT(waves, method='trap', rise=rise, top=top, tau=tau, percentage=percentage, pretrigger = pretrigger, shift = shift, mean = mean, batchSize = batchSize)
			return np.array(results.real), np.array(results.imag)
		else: #in this case just use the CPU
			#first define the filter itself
			filtFunc = None
			recursiveFunc = None
			if filt == 'trap':
				recursiveFunc = trapezoidalFilterRecursive
				filtFunc = extractTrapResults
			elif filt == 'cusp':
				recursiveFunc = cuspFilterRecursive
				filtFunc = extractCuspResults
			else:
				print('filter type not understood:', filt)
				return None, None
			means = waves[:,0:pretrigger].mean(axis=1)
			shifted = np.subtract(waves, means[:,None])
			filtered = np.apply_along_axis(recursiveFunc, 1, shifted, rise, top, tau)
			res = np.apply_along_axis(filtFunc, 1, filtered, rise, top, percentage, shift, mean)
			eners = res[:,0]
			times = res[:,1]
			return np.array(eners), np.array(times)
	
	
def testFIRFilterFFT(waves, rise, top, tau, filt = 'trap',  pretrigger=800, batchSize = 1000, useGPU = False, useFFTW = True, percentage = 0.8, mean = 0, shift = 0):
	"""
	This function applies either the trapezoidal filter, or cusp filter, to a batch of waveforms and extracts energy and timing information with it. This version of the filter is defined via convolutions instead of the standard recursive method to improve performance in Python.
	For a description of the trapezoidal filter defined here, check this resource.
	Nuclear Instruments and Methods in Physics Research A353 (1994) 261-264
	https://deepblue.lib.umich.edu/bitstream/handle/2027.42/31113/0000009.pdf?sequence=1
	
	Parameters
	----------
	waves: np.ndarray
		a 2d numpy array with waveforms in it
	rise: int
		the risetime parameter for the trapezoidal filter
	top: int
		the flat top length of the trapezoidal filter
	tau: int,float
		the decay rate parameter for the trapezoidal filter
	filt: string
		The filter to be used. Supports either 'cusp' or 'trap' options
	pretrigger: int
		the length of the waveform to be used to average the baseline to 0
	batchSize: int
		how many waveforms to process at a time. Defaults to 1,000 but should be optimized on a per-computer basis. -1 means do all at once (warning this can use a lot of RAM)
	useGPU: bool
		Changes the code to operate on the GPU instead through Cupy library and Numba GPU compilation
	useFFTW: bool
		Uses the FFTW library instead. Only applies when running on the CPU
	percentage: float between 0 and 1, defaults to 0.8
		The threshold cross percentage
	mean: int, defaults to 0
		The number of values to average over for energy extraction
	shift: int (defaults to 0)
		applies a shift to the extraction location for the energy from the trap filter
	
	Returns
	-------
	energy: list
		list of floating point numbers that define the energies of the waveforms
	timing: list
		list of extracted start time of waveforms
	"""
	if waves.shape[0] == 0:
		return [], []
	else:
		pretrigger = int(pretrigger)
		#this applies the trapezoidal filter to the waveforms and grabs the energy and timing results
		eners = []
		times = []
		#do the bulk wave preparation
		if batchSize == -1:
			batchSize = len(waves)
		numWaves = len(waves)
		leftover = numWaves % batchSize
		numbatches = int(numWaves / batchSize)
		#set up the main computations
		if useGPU == True and __gpuAvailable == True: #only try it when it's available and requested
			results = gpu.gpuTrapCuspFilterImplementationFFT(waves, method='trap', rise=rise, top=top, tau=tau, percentage=percentage, pretrigger = pretrigger, shift = shift, mean = mean, batchSize = batchSize)
			return np.array(results.real), np.array(results.imag)
		else: #in this case just use the CPU
			#first define the filter itself
			filtLen = rise * 2 + top
			padLen = filtLen + waves.shape[1] - 1
			padLen = int(pow(2, np.ceil(np.log2(padLen)))) #pad to be the next power of 2, cause it's faster
			filtFunc = None
			if filt == 'trap':
				filt = defineSingleTrap(rise, top, tau, padLen)
				filtFunc = extractTrapResults
			elif filt == 'cusp':
				filt = defineCuspFilter(rise, top, tau, padLen)
				filtFunc = extractCuspResults
			else:
				print('filter type not understood:', filt)
				return None, None
			if __fftwAvailable and useFFTW: #if both it's available and the user requests to use it
				#now pad the array to be the right length
				fftFilt = np.transpose(np.fft.rfft(filt),None)
				
				results = pyFFTWExtraction(waves, pretrigger, fftFilt, padLen, extractTrapResults, [rise, top, percentage, shift, mean], batchSize = batchSize)
				return results[:,0], results[:,1]
			else:
				if batchSize == -1 or batchSize > numWaves: #in the case we want to do the whole process in one batch
					means = waves[:,0:pretrigger].mean(axis=1)
					shifted = np.subtract(waves, means[:,None])
					padded = np.zeros(shape=(waves.shape[0], padLen), dtype=waves.dtype)
					padded[:,:waves.shape[1]] = shifted[:,:]
					fftFilt = np.asarray(np.transpose(np.fft.rfft(filt),None))
					fftWaves = np.fft.rfft(padded, axis=1)
					multiplication = np.multiply(fftWaves, fftFilt)
					filtered = np.fft.irfft(multiplication, axis=1).real
					res = np.apply_along_axis(filtFunc, 1, filtered, rise, top, percentage, shift, mean)
					eners = res[:,0]
					times = res[:,1]
				else:
					numBatches = numWaves//batchSize
					if numWaves % batchSize != 0:
						numBatches += 1
					#prep the work environment for the stuff
					#create the buffers that'll be used for the FFTs
					padded = np.zeros(shape=(batchSize, padLen), dtype=waves.dtype)
					fftArray = None
					if waves.dtype == np.float32:
						fftArray = np.zeros(shape=[batchSize, padLen//2+1], dtype=np.complex64)
					else:
						fftArray = np.zeros(shape=[batchSize, padLen//2+1], dtype=np.complex128)
					#create the results array 
					results = np.zeros(shape=(numWaves, 2))
					fftFilt = np.asarray(np.transpose(np.fft.rfft(filt),None))
					for i in range(numBatches):
						startLoc = batchSize * i
						stopLoc = batchSize * (i+1)
						if stopLoc > numWaves:
							stopLoc = numWaves
						padded[:stopLoc-startLoc,:waves.shape[1]] = waves[startLoc:stopLoc,:] 
						means = padded[:stopLoc-startLoc,0:pretrigger].mean(axis=1)
						padded[:stopLoc-startLoc,:waves.shape[1]] = np.subtract(padded[:stopLoc-startLoc,:waves.shape[1]], means[:,None])
						fftWaves = np.fft.rfft(padded, axis=1) #do their FFT, now the FFT results are stored in the fftArray array
						np.multiply(fftWaves, fftFilt, out=fftArray) #do the multiplication in place
						filtered = np.fft.irfft(fftArray, axis=1).real
						#fft has now been applied, apply the extraction function
						res = np.apply_along_axis(filtFunc, 1, filtered, rise, top, percentage, shift, mean)
						results[startLoc:stopLoc,0] = np.copy(res[:stopLoc-startLoc,0])
						results[startLoc:stopLoc,1] = np.copy(res[:stopLoc-startLoc,1])
						padded[:,:] = 0 #reset it to 0
					eners = results[:,0]
					times = results[:,1]
		return np.array(eners), np.array(times)

def calculatePseudoInverse(a, weight = None):
	if a.ndim == 1: #in the case the input is just a vector
		length = len(a)
		if weight is None:
			scale = np.dot(a, a)
			out = np.zeros(len(a))
			for i in range(len(a)):
				if abs(a[i]) < 0.00000001:
					out[i] = 0
				else:
					out[i] = a[i]/scale
			return out
		else:
			w = None
			if weight.ndim == 1:
				w = np.zeros((length, length))
				for i in range(length):
					w[i,i] = weight[i]
			else:
				w = np.copy(weight)
			ATW = np.matmul(np.transpose(a), w)
			ATWA = np.matmul(ATW, a)
			ATWAInv = np.inverse(ATWA)
			inverse = np.matmul(ATWAInv, ATW)
			return inverse
	elif a.ndim == 2: #in this case it's a matrix
		if weight is None:
			return np.linalg.pinv(a)
		else:
			w = None
			if weight.ndim == 1:
				w = np.zeros((a, a))
				for i in range(length):
					w[i,i] = weight[i]
			else:
				w = np.copy(weight)
			ATW = np.matmul(np.transpose(a), w)
			ATWA = np.matmul(ATW, a)
			ATWAInv = np.inverse(ATWA)
			inverse = np.matmul(ATWAInv, ATW)
			return inverse
	else:
		print('Error: invalid input dimension, needs to be 1d or 2d')
		return None

def calcATWA(matrix, weight = None):
	if weight is None:
		return np.matmul(np.transpose(matrix), matrix)
	w = None
	if weight.ndim == 1:
		w = np.zeros((length, length))
		for i in range(length):
			w[i,i] = weight[i]
	else:
		w = np.copy(weight)
	return np.matmul(matrix, np.matmul(weight, matrix))

@numba.jit
def __pseudoFitPSDAlongAxis(waves, maxLocs, fitMatrix, pseudoInverse, filtert0, filterLength, searchRange, t0Weight, numIdeal, returnResiduals):
	shape = None
	if numIdeal > 1: #if there are multiple
		if returnResiduals:
			shape = (len(waves), pseudoInverse.shape[1]+3+pseudoInverse.shape[0])
		else:
			shape = (len(waves), pseudoInverse.shape[1]+3)
	else:
		if returnResiduals:
			shape = (len(waves), pseudoInverse.shape[1]+2+pseudoInverse.shape[0])
		else:
			shape = (len(waves), pseudoInverse.shape[1]+2)
	results = np.zeros(shape, dtype=np.float64)
	results[:] = np.nan #set all values to failure
	for w in range(len(waves)):
		res = results[w,:]
		wave = waves[w]
		maxLoc = maxLocs[w]
		fitRegionLeft = int(maxLoc - filterLength - searchRange)
		fitRegionRight = maxLoc + searchRange
		res = np.zeros(shape[1])
		if fitRegionLeft < 0 or fitRegionRight >= len(wave): #if the fitting failed to return something in the valid window, return np.nan for all values
			continue
		fitRegion = wave[maxLoc-filterLength - searchRange: maxLoc + searchRange]
		fitParameters = np.zeros((searchRange*2+1, pseudoInverse.shape[1]))
		minChi = np.inf
		minChiLoc = -1
		for i in range(searchRange * 2 + 1):#determine fit parameters, fitted wave, and chi squared in one loop
			for j in range(pseudoInverse.shape[1]): #fit parameters first
				fitParameters[i,j] = np.dot(pseudoInverse[:,j], fitRegion[i:filterLength+i])
			fittedWave = np.dot(fitMatrix, fitParameters[i,:])
			waveDiff = fitRegion[i:filterLength+i]-fittedWave
			chi = None
			if t0Weight is None:
				chi = np.sum(np.square(waveDiff))
			else:
				if t0Weight.ndim == 1:
					chi = np.dot(waveDiff, np.multiply(t0Weight, waveDiff))
				else:
					chi = np.dot(waveDiff, np.dot(t0Weight, waveDiff))
			if chi < minChi:
				minChi = chi
				minChiLoc = i
		t0 = maxLoc - filterLength + filtert0 - searchRange + minChiLoc
		#now with the best time figured out, grab the best fit parameters as well
		bestFitParameters = fitParameters[minChiLoc, :]
		res[:len(bestFitParameters)] = bestFitParameters
		res[-2]= t0
		res[-1] = minChi/(filterLength - len(bestFitParameters))
		results[w,:] = res[:]
	return results

def __pseudoFitBlockFunction(waveforms, padLen, maxParameterFilterFFT, fitMatrix, pseudoInverse, filtert0, 
							 filterLength, searchRange, t0Weight, numIdeal, batchSize, returnResiduals = False):
	"""
	This is a more efficient form of the process using FFTW
	"""
	numWaves, waveformLength = waveforms.shape
	badRegionStart = fitMatrix.shape[0]
	badRegionStop = waveformLength - fitMatrix.shape[0]
	if batchSize <= 0 or batchSize >= numWaves:
		batchSize = numWaves
	numBatches = numWaves//batchSize
	if numWaves % batchSize != 0:
		numBatches += 1
	#prep the work environment for the stuff
	#create the buffers that'll be used for the FFTs
	filterLength = pseudoInverse.shape[0]
	padded = np.zeros(shape=(batchSize, padLen), dtype=np.float32)
	fftArray = np.zeros(shape=(batchSize, padLen//2+1), dtype=np.complex64)
	#create the FFT plans
	fftForward = pyfftw.FFTW(padded, fftArray, direction='FFTW_FORWARD', axes=[1])
	fftReverse = pyfftw.FFTW(fftArray, padded, direction='FFTW_BACKWARD', axes=[1])
	#create the results array 
	shape = np.zeros(2, dtype=int)
	shape[0] = len(waveforms)
	if numIdeal > 1: #if there are multiple
		if returnResiduals:
			shape[1] =  pseudoInverse.shape[1]+3+pseudoInverse.shape[0]
		else:
			shape[1] = pseudoInverse.shape[1]+3
	else:
		if returnResiduals:
			shape[1] = pseudoInverse.shape[1]+2+pseudoInverse.shape[0]
		else:
			shape[1] = pseudoInverse.shape[1]+2
	results = np.zeros(shape)
	for i in range(numBatches):
		padded[:,:] = 0
		startLoc = batchSize * i
		stopLoc = batchSize * (i+1)
		if stopLoc > numWaves:
			stopLoc = numWaves
		padded[:stopLoc-startLoc,:waveformLength] = waveforms[startLoc:stopLoc,:]
		#this time we don't have to subtract the baseline or anything like that
		#we can just do the normal thing and it'll work nicely
		fftForward() #do their FFT, now the FFT results are stored in the fftArray array
		fftArray *= maxParameterFilterFFT #do the multiplication in place or as in place as python allows
		fftReverse() #now undo the FFT back
		maxLocs = np.argmax(padded[:,badRegionStart:badRegionStop], axis=1)+badRegionStart
		results[startLoc:stopLoc] = __pseudoFitPSDAlongAxis(waveforms[startLoc:stopLoc,:].astype(np.float64, copy=False), maxLocs, fitMatrix, pseudoInverse, filtert0, filterLength, searchRange, t0Weight, numIdeal, returnResiduals)
	return results

def pseudoInverseFit(waves, templates, baselines, filtert0 = None, searchRange = 10, t0Weight = None, returnResiduals = False, batchSize = 1000):
	'''
	This is the pseudoinverse fitting method based on an upcoming paper. Talk with David Mathews for
	details of it's implementation. This method fits a waveform with a series of template functions that the
	user provides to the code. Depending on the values provided, it returns different information to the user.
	
	Parameters
	----------
	waves: np.ndarray or dask.array
		The waveforms to be processed. 
	templates: list, np.ndarray (must share n value with baselines)
		a list of the template functions. 
		It is expected to be in the arrangement (t, n) where t is the number of template functions and n is their length
	baselines: list, np.ndarray (must share n value with templates)
		a list of the baseline functions
		Is is expected to be in the arrangement (b, n) where b is the number of baseline functions and n is their length
	filtert0: None, int
		If none, assumes the start time of the template functions provided is in the middle
		If int, that defines the start postion of the waveform
	searchRange: int (defaults to 10):
		the number of datapoints on either side of the predicted t0 location to search for the minimum chi squared value
		Larger values increase computational time
	t0Weight: None, np.ndarray
		If none, an unweighted fit is used to extract t0
		If np.ndarray.ndim = 1: a linear weight is used, no off axis elements
		If np.ndarray.ndim = 2: a full 2d weighting matrix is used
			The matrix must be hermitian if this is used otherwise the algorithm isn't properly defined
	returnResiduals: False
		Controls if the residuals of the fit are returned or not
	batchSize: int
		how many waveforms to process at a time. Defaults to 1,000 but should be optimized on a per-computer basis. -1 means do all at once (warning this can use a lot of RAM)
	Returns
	-------
	If the fitting fails, all returned values for that waveform are np.nan
	
	best fit parameters: np.ndarray
		a matrix that is [n, t+b] containing the best fit parameters that were found for each waveform
	t0s: np.ndarray
		an array that is n long containing the extracted start time of the waveform.
		This is the location of the best fit as determined by chi squared minimization
	minChis: np.ndarray
		an array that is n long containing the extracted minimum chi squared value
		The degrees of freedom have been divided out but error/uncertainty in each data point has not been
		Requires division by the variance before it can be considered reduced chi squared
	'''
	#check the inputs to see if they make sense
	if waves.shape[0] == 0:
		return [], [], []
	else:
		#do the bulk wave preparation
		if batchSize == -1:
			batchSize = len(waves)
		numWaves = len(waves)
		#now convert the various templates and baseline functions to arrays if they aren't already
		if isinstance(templates, list):
			templates = np.asarray(templates)
		if isinstance(baselines, list):
			baselines = np.asarray(baselines)
		if templates.shape[1] != baselines.shape[1]:
			return [], [], []
		if filtert0 is None: #assume it's in the middle then
			filtert0 = templates.shape[1]//2
		length = waves.shape[1]
		filterLength = templates.shape[1]
		filterLength = templates.shape[1]
		padLen = length + filterLength - 1
		padLen = int(pow(2, np.ceil(np.log2(padLen))))
		#first we need to figure out the fit matrix
		numIdeal = len(templates)
		numBaseline = len(baselines)
		fitMatrix = []
		for shape in templates:
			fitMatrix.append(shape)
		for shape in baselines:
			fitMatrix.append(shape)
		fitMatrix = np.asarray(fitMatrix)
		#now we need to calculate the psuedoinverse
		pseudoInverse = calculatePseudoInverse(fitMatrix, weight = t0Weight)
		#first make the filter that determines the fit parameter maximum location as that is the fastest way
		maxParameterFilter = np.zeros(padLen) #full length of the waveforms for fft convolution
		maxParameterFilter[:pseudoInverse.shape[0]] = np.flip(np.mean(pseudoInverse[:, 0:numIdeal], axis=1)) #grab the mean of the inverse filters for each ideal template
		maxParameterFilterFFT = np.fft.rfft(maxParameterFilter) #do the fft of it to reduce computations later
		#now we know the region where the chi square minimum should approximately be, do the full process of finding fit parameters in this region now
		results = __pseudoFitBlockFunction(waves, padLen, maxParameterFilterFFT, fitMatrix.T, pseudoInverse, filtert0, filterLength, searchRange, t0Weight, numIdeal, batchSize, returnResiduals)
		bestParameters = results[:,:pseudoInverse.shape[1]]
		t0s = results[:,pseudoInverse.shape[1]]
		minChis = results[:,pseudoInverse.shape[1]+1]
		return bestParameters, t0s, minChis