# slidingLeastSquaresFitting



## Introduction
This repository stores some example code for the Sliding Least Squares fit routine described in this paper: [REFERENCE]. The purpose of this repository is to demonstrate some of the features and capabilities of this routine, while also documenting the source code used during testing for the paper. 

This code is defined in 2 source files, slidingFittingFunctions.py and gpuFunctions.py. The DemonstrationNotebook.ipynb notebook calls the functions defined in those source files and demonstrates the method from start to finish on simulated datasets. 

## Dependencies
### CPU-Only
For systems with only CPUs, these are the libraries that are required.
- Numpy - https://numpy.org/
- Numba - https://numba.pydata.org/
- matplotlib - https://matplotlib.org/
- scipy - https://scipy.org/
- pyFFTW - https://github.com/pyFFTW/pyFFTW

### GPU Version
For systems where GPU deployment is desireable, all of the CPU libraries are required as well as the following. The code presented here has been tested on both Nvidia and AMD GPUs through Cupy.
- cupy - https://cupy.dev/
