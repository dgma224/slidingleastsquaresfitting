import slidingFittingFunctions as fitLib
import matplotlib.pyplot as plt
import numpy as np
import numba
from scipy.optimize import curve_fit
import time
import pyfftw
import cupy as cp

#first make a large test dataset
#this needs to be a fairly large dataset to be a proper test, particularly for the batched methods and GPU
numWaveforms = 20000
signalLength = 10000
t0 = 5000
decay = 1000
amplitude = 500

rms = 10
noise = 'white'

dataset = np.zeros((numWaveforms, signalLength), dtype=np.float32)
for i in range(numWaveforms):
	dataset[i,:] = fitLib.simpleSignalGenerator(signalLength, t0, decay, amplitude, noise, rms)

#configure the template functions for the fitting routine
fitLength = 1000
fitT0 = 500
order = 0 #the order of the baseline polynomial

templates = []
templates.append(fitLib.simpleSignalGenerator(fitLength, fitT0, decay, 1.0, None, 0))
templates = np.array(templates)

baselines = []
for i in range(order+1):
	x = np.arange(fitLength)/fitLength
	baselines.append(np.power(x, i))
baselines = np.array(baselines)

#define the trap and cusp filter parameters to be used during testing
rise = 1000
top = 100
tau = 1000

batchSize = 1024

datasetGPU = cp.asarray(dataset)

start = time.time()
_ = fitLib.gpu.pseudoInverseFitGPU(datasetGPU[:10], templates, baselines, batchSize = batchSize)
pseudoinverseTimeGPU = (time.time()-start)/len(dataset)
print('pseudoinverse gpu: ', pseudoinverseTimeGPU)

start = time.time()
_ = fitLib.gpu.pseudoInverseFitGPU(datasetGPU, templates, baselines, batchSize = batchSize)
pseudoinverseTimeGPU = (time.time()-start)/len(dataset)
print('pseudoinverse gpu: ', pseudoinverseTimeGPU)
